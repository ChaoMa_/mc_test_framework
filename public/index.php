<?php
use System\Src\WebFramework;
use System\Lib\Logger;
use josegonzalez\Dotenv\Loader;
use System\Lib\BaseScript;

define('_ROOT', dirname(dirname(__FILE__)));

session_start();

if (function_exists('date_default_timezone_set')) {
    @date_default_timezone_set('Asia/Shanghai');
}

$autoload = require _ROOT . '/vendor/autoload.php';

// 设置自动载入类的路径
$autoload->setPsr4('App\\', [
    _ROOT . '/App/'
]);

$autoload->setPsr4('System\\', [
    _ROOT . '/System/'
]);

Loader::load(_ROOT . '/.env')->toEnv();

if ($_ENV['APP_DEBUG']) {

    function reportCoverage()
    {
        $arrCoverage = xdebug_get_code_coverage();
        xdebug_stop_code_coverage();
        $time = intval(microtime(true) * 1000);
        $filepath = _ROOT . '/' . $_ENV['COV_ROOT'];
        $filename = $filepath . '/' . $time . '_' . posix_getpid() . '.cov';
        if (! file_exists($filepath)) {
            mkdir($filepath, 0755, true);
        }
        file_put_contents($filename, serialize($arrCoverage));
    }
    
    if (extension_loaded('xdebug')) {
        if ($_ENV['COVERAGE']) {
            xdebug_start_code_coverage(XDEBUG_CC_UNUSED | XDEBUG_CC_DEAD_CODE);
            register_shutdown_function('reportCoverage');
        }
    }
}

if (isset($argv[1])) {
    BaseScript::main();
} else {
    Logger::init(_ROOT . $_ENV['APP_LOG_FILE'], $_ENV['APP_LOG_LEVEL']);
    
    class_alias('\\System\Lib\Logger', 'Logger');
    
    $app = WebFramework::getInstance();
    
    require _ROOT . '/App/router.php';
    
    $app->start();
}
/* vim: set ts=4 sw=4 sts=4 tw=100 noet:*/
