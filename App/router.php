<?php
namespace App;

$app->addRoute('get', '/', 'Test\index');
$app->addRoute('get', '/db', 'Test\db');
$app->addRoute('get', '/params/{test:.+}', 'Test\params');
$app->addRoute('get', '/cache/{id:.+}', 'Test\cache');
$app->addRoute('get', '/language', 'Test\language');
