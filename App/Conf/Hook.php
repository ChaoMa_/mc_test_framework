<?php
namespace App\Conf;

/**
 * hook的配置
 *
 * @author machao
 *        
 */
class Hook
{

    public static $before = [
        'Test'
    ];

    public static $after = [];

    public static $module_before = [];

    public static $module_after = [];
}
