<?php
namespace App\Conf;

class Redis
{

    public static $servers = [
        1 => [
            '127.0.0.1'
        ]
    ];

    const PORT = 6378;
}