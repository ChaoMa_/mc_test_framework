<?php
namespace App\Controllers\Test;

use System\Lib\CacheProxy;
use App\Modules\Test\TestLogic;
use System\Src\WebFramework;
use System\Lib\Util;
use System\Lib\Language;

class Test
{

    public function index()
    {
        return 'aaa';
    }

    public function db()
    {
        WebFramework::getInstance()->setText();
        $result = TestLogic::test();
        return $result->username;
    }

    public function params($arrRequest)
    {
        WebFramework::getInstance()->setText();
        $id = Util::getParameter($arrRequest, 'test');
        return $id;
    }

    public function cache($arrRequest)
    {
        WebFramework::getInstance()->setText();
        CacheProxy::set('aa', $arrRequest['id']);
        return CacheProxy::get('aa') . 'cache';
    }

    public function language()
    {
        WebFramework::getInstance()->setText();
        
        return Language::getInstance()->getString('测试');
    }
}