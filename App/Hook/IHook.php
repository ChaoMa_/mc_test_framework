<?php
namespace App\Hook;

/**
 * hook接口文件
 *
 * @author machao
 *        
 */
interface IHook
{

    public static function execute(Array &$arrRequest);
}