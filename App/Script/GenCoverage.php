<?php
namespace App\Script;

use System\Lib\BaseScript;

class GenCoverage extends BaseScript
{

    public function executeScript($arrOption)
    {
        $coverage = new \PHP_CodeCoverage();
        $coverage->filter()->addDirectoryToWhitelist(_ROOT . '/App/');
        $coverage->filter()->addDirectoryToWhitelist(_ROOT . '/System/');
        $coverage->filter()->addDirectoryToWhitelist(_ROOT . '/public/');
        
        $cov_root = _ROOT . $_ENV['COV_ROOT'];
        $dir = opendir($cov_root);

        if (empty($dir)) {
            echo $cov_root . " can't be opened, please check if it exists\n";
            return;
        }

        $arrCoverage = array();
        $arrFile = array();
        while (true) {
            $file = readdir($dir);
            if (empty($file)) {
                break;
            }
            if ($file == '.' || $file == '..') {
                continue;
            }
            $file = $cov_root . '/' . $file;
            $arrFile[] = $file;
            $data = file_get_contents($file);
            $arrData = unserialize($data);
            if (empty($arrCoverage)) {
                $arrCoverage = $arrData;
                continue;
            }
            
            foreach ($arrData as $file => $arrLine) {
                if (! isset($arrCoverage[$file])) {
                    $arrCoverage[$file] = $arrLine;
                    continue;
                }
                
                foreach ($arrLine as $line => $flag) {
                    if (! isset($arrCoverage[$file][$line])) {
                        $arrCoverage[$file][$line] = $flag;
                        continue;
                    }
                    
                    if ($arrCoverage[$file][$line] < $flag) {
                        $arrCoverage[$file][$line] = $flag;
                    }
                }
            }
        }
        
        if (count($arrFile) > 1) {
            $time = intval(microtime(true) * 1000);
            $filename = $cov_root . '/' . $time . '_' . posix_getpid() . '.cov';
            file_put_contents($filename, serialize($arrCoverage));
            
            foreach ($arrFile as $file) {
                unlink($file);
            }
        }
        $coverage->append($arrCoverage, 'cov');
        $generator = new \PHP_CodeCoverage_Report_HTML();
        $generator->process($coverage, _ROOT . $_ENV['COVERAGE_ROOT']);
        echo sprintf("generate cov file in '%s' to '%s', done\n", $cov_root, _ROOT . $_ENV['COVERAGE_ROOT']);
    }
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
