<?php
namespace App\Modules\Test;

use System\Lib\DBProxy;

/**
 * 数据库访问
 */
class TestDao
{

    public static function test()
    {
        $db = DBProxy::getDB();
        return $result = $db->select('*')
            ->from('admin_user')
            ->fetch();
    }
}