<?php
namespace App\Modules\Test;

/**
 * 测试内部需实现接口
 *
 * @author machao
 *        
 */
interface ITest
{

    public static function test();
}