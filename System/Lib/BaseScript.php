<?php
namespace System\Lib;

class OptionType
{

    const NOARG = 1;

    const REQUIRED = 2;

    const OPTIONAL = 3;
}

/**
 * 脚本基类
 *
 * @author hoping
 *        
 */
abstract class BaseScript
{

    const USAGE = 'usage: php ROOT/lib/ScriptRunner.php -f -b -e -c -h -d
其中各参数含义如下：
-f 指定需要运行的脚本文件名
-b 指定起始时间
-e 指定结束时间
-c 指定需要运行的脚本文件中的执行类名
-h 打印本段内容
-d 日志级别 ';

    /**
     * 起始时间
     *
     * @var string
     */
    protected $beginTime;

    /**
     * 结束时间
     *
     * @var string
     */
    protected $endTime;

    /**
     * 运行的脚本名字
     */
    protected $className;

    /**
     * 日志id
     *
     * @var string
     */
    protected $logid;

    /**
     * 当前时间
     *
     * @var int
     */
    protected $time;

    /**
     * 是否是DEBUG模式
     *
     * @var bool
     */
    protected $logLevel;

    /**
     * 执行的脚本名
     *
     * @var
     *
     */
    public $sname;

    /**
     * 保存结果的文件名
     *
     * @var
     *
     */
    public $logName;

    /**
     * 脚本的执行函数
     */
    public function execute($arrOption)
    {
        $err = "ok";
        try {
            $this->init($arrOption);
            $this->initLogger();
            
            $this->executeScript($arrOption);
        } catch (\Exception $e) {
            $err = $e->getMessage();
            if ($err != 'fake' || $_ENV['SCRIPT_DEBUG']) {
                Logger::fatal("uncaught \Exception:%s", $e->getMessage());
                Logger::info("%s", $e->getTraceAsString());
            }
        }
        return $err;
    }

    /**
     * 错误处理函数
     *
     * @param int $errcode            
     * @param string $errstr            
     * @param string $errfile            
     * @param int $errline            
     * @param array $errcontext            
     */
    public function errorHandler($errcode, $errstr, $errfile, $errline, $errcontext)
    {
        if (! $errcode & error_reporting()) {
            return true;
        }
        
        Logger::fatal('errcode:%d, errstr:%s, errfile:%s, errline:%s', $errcode, $errstr, $errfile, $errline);
        Logger::trace('error context:%s', $errcontext);
        throw new \Exception('php');
    }

    /**
     * 增加一个配置
     *
     * @param array $arrConfig            
     * @param string $config            
     * @param string $value            
     */
    static private function addConfig(&$arrConfig, $config, $value)
    {
        if (isset($arrConfig[$config])) {
            if (is_array($arrConfig[$config])) {
                $arrConfig[$config][] = $value;
            } else {
                $arrConfig[$config] = array(
                    $arrConfig[$config],
                    $value
                );
            }
        } else {
            $arrConfig[$config] = $value;
        }
        unset($arrConfig);
    }

    /**
     * 根据参数解析输入配置
     *
     * @param array $arrArg            
     * @return array
     */
    static protected function getOption($arrArg, $option, $offset = 0)
    {
        $arrOption = array();
        for ($counter = 0; $counter < strlen($option); $counter ++) {
            $config = $option[$counter];
            if ($config == ':') {
                Logger::fatal('invalid option:%s at %d', $option, $counter);
                throw new \Exception('config');
            }
            
            $arrOption[$config] = OptionType::NOARG;
            if (isset($option[$counter + 1]) && $option[$counter + 1] == ':') {
                $counter ++;
                $arrOption[$config] = OptionType::REQUIRED;
                if (isset($option[$counter + 1]) && $option[$counter + 1] == ':') {
                    $counter ++;
                    $arrOption[$config] = OptionType::OPTIONAL;
                }
            }
        }
        
        $arrArg = array_merge($arrArg);
        $arrRet = array(
            'args' => array()
        );
        for ($counter = $offset; $counter < count($arrArg); $counter ++) {
            $config = trim($arrArg[$counter]);
            if ($arrArg[$counter][0] == '-') {
                $config = trim($config, '-');
                if (! isset($arrOption[$config])) {
                    $arrRet['args'][] = $arrArg[$counter];
                    continue;
                }
                
                switch ($arrOption[$config]) {
                    case OptionType::NOARG:
                        self::addConfig($arrRet, $config, true);
                        break;
                    case OptionType::REQUIRED:
                        $counter ++;
                        if (! isset($arrArg[$counter]) || $arrArg[$counter][0] == '-') {
                            Logger::fatal("option %s requires arg", $config);
                            throw new \Exception('config');
                        }
                        
                        self::addConfig($arrRet, $config, $arrArg[$counter]);
                        break;
                    case OptionType::OPTIONAL:
                        if (isset($arrArg[$counter + 1]) && $arrArg[$counter + 1][0] != '-') {
                            $counter ++;
                            self::addConfig($arrRet, $config, $arrArg[$counter]);
                        } else {
                            self::addConfig($arrRet, $config, true);
                        }
                        break;
                    default:
                        Logger::fatal("undefined option type:%d", $arrOption[$config]);
                        throw new \Exception('config');
                }
            } else {
                $arrRet['args'][] = $config;
            }
        }
        return $arrRet;
    }

    /*
     * (non-PHPdoc) @see BaseScript::initLogger()
     */
    protected function initLogger()
    {
        Logger::init(_ROOT . $_ENV['SCRIPT_LOG'], $this->logLevel);
        Logger::addBasic('logid', $this->logid);
        Logger::addBasic('scriptName', $this->className);
        if (! empty($this->beginTime)) {
            Logger::addBasic('beginTime', $this->beginTime);
        }
        if (! empty($this->endTime)) {
            Logger::addBasic('endTime', $this->endTime);
        }
    }

    public function defaultInit($beginTime, $endTime, $logid, $logLevel, $clazz)
    {
        set_error_handler(array(
            $this,
            'errorHandler'
        ));
        $this->beginTime = $beginTime;
        $this->endTime = $endTime;
        $this->logid = $logid;
        $this->logLevel = $logLevel;
        $this->className = $clazz;
    }

    public function init($arrOption)
    {}

    /**
     * 记录日志
     *
     * @param string $data            
     */
    static protected function log($data, $file = '')
    {
        if ($file != '') {
            $file = __DIR__ . '/' . $file . '.log';
            $f = fopen($file, "a");
            fwrite($f, $data . "\n");
            fclose($f);
        } else {
            file_put_contents('php://stderr', $data . "\n");
        }
    }

    /**
     * 主执行函数
     */
    public static function main()
    {
        error_reporting(E_ALL | E_STRICT);
        global $argc, $argv;
        
        $arrOption = BaseScript::getOption($argv, 'f::b::e::c::g::s::h::d::i', 1);
        if (isset($arrOption['h']) || ! isset($arrOption['f'])) {
            BaseScript::log(BaseScript::USAGE);
            return;
        }
        
        $file = _ROOT . '/App/Script/' . $arrOption['f'];
        if (pathinfo($file, PATHINFO_EXTENSION) != 'php') {
            $file = _ROOT . '/App/Script/' . $arrOption['f'] . '.php';
        }
        if (! file_exists($file)) {
            $file = $arrOption['f'];
            if (! file_exists($file)) {
                BaseScript::log("file $file not found");
                return;
            }
        }
        
        require_once ($file);
        if (isset($arrOption['c'])) {
            $clazz = $arrOption['c'];
        } else {
            $name = basename($file);
            $arrName = explode('.', $name);
            $clazz = $arrName[0];
        }
        
        $clazz = '\App\Script\\'.$clazz;
        
        if (isset($arrOption['d']) && Logger::checkLogLevel(intval($arrOption['d']))) {
            $logLevel = intval($arrOption['d']);
        } else {
            $logLevel = $_ENV['SCRIPT_LOG_LEVEL'];
        }
        
        if (isset($arrOption['b'])) {
            $beginTime = $arrOption['b'];
        } else {
            $beginTime = '';
        }
        
        if (isset($arrOption['e'])) {
            $endTime = $arrOption['e'];
        } else {
            $endTime = '';
        }
        
        $logid = Util::genLogId();
        
        $startTime = microtime(true);
        
        $err = 'ok';
        if (! class_exists($clazz)) {
            BaseScript::log("class $clazz not found");
            $err = 'clazz';
        } else {
            $scripter = new $clazz();
            if (! $scripter instanceof BaseScript) {
                BaseScript::log("script $file is not subclass of BaseScript");
                $err = 'extends';
            } else {
                $scripter->sname = $clazz;
                $scripter->logName = date("Y-m-d_H-i-s") . ".log";
                if (isset($arrOption['i'])) {
                    $info = $scripter->getInfo();
                    $str = "\n中文名称:{$info['CNName']}\n脚本说明：" . $info['explain'] . "\n参数说明：\n";
                    foreach ($info['param'] as $key => $v) {
                        $str .= "参数{$key}:{$v}\n";
                    }
                    BaseScript::log($str);
                    return;
                }
                
                try {
                    $scripter->defaultInit($beginTime, $endTime, $logid, $logLevel, $clazz);
                    $startMethodTime = microtime(true);
                    
                    //ob_start();
                    $err = $scripter->execute($arrOption['args']);
                    $_log = ob_get_contents();
                    //ob_end_clean();
                    BaseScript::log($_log);
                    $endMethodTime = microtime(true);
                } catch (\Exception $e) {
                    Logger::fatal("uncaught \Exception:%s for method:%s", $e->getMessage(), $scripter->getMethod());
                    Logger::info('%s', $e->getTraceAsString());
                    $endMethodTime = microtime(true);
                    $err = $e->getMessage();
                }
            }
        }
        
        $totalCost = intval(($endMethodTime - $startTime) * 1000);
        $methodCost = intval(($endMethodTime - $startMethodTime) * 1000);
        $frameCost = $totalCost - $methodCost;
        Logger::notice("method:%s, err:%s, total cost:%d(ms), framework cost:%d(ms), method cost:%d(ms), request size:%d(byte), response size:%d(byte)", $scripter->getMethod(), $err, $totalCost, $frameCost, $methodCost, 0, 0);
    }

    /**
     *
     * @param string $str
     *            保存的字符串
     * @return int
     */
    public function logResult($str)
    {
        $fileDir = _ROOT. '/App/Script/' . $this->sname;
        if (! file_exists($fileDir)) {
            mkdir($fileDir);
        }
        $file = $fileDir . '/' . $this->logName;
        $ret = file_put_contents($file, $str . "\n", FILE_APPEND);
        return $ret;
    }

    /**
     * 返回运行脚本的信息
     *
     * @return mixed
     * @throws \Exception
     */
    public function getInfo()
    {
        if (isset($this->info) && ! empty($this->info)) {
            return $this->info;
        } else {
            Logger::fatal("this script's info is not set.");
            throw new \Exception("this script's info is not set");
        }
    }

    /**
     * 实际的执行函数
     */
    protected abstract function executeScript($arrOption);

    public function getMethod()
    {
        return get_class($this);
    }
}
/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
