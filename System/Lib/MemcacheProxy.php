<?php
namespace System\Lib;

use Flexihash\Flexihash;

class MemcacheProxy implements ICacheProxy
{

    private static $Flexihash = null;

    private static $connects = array();

    /**
     * 获取当前cache对象
     * 
     * @throws \Exception
     * @return Memcache
     */
    public function getCache($key)
    {
        $config = '\App\Conf\\' . $_ENV['CACHE_DRIVER'];
        
        $host = null;
        
        $port = null;
        
        if (is_null(self::$Flexihash)) {
            // 获取一致hash实例
            $this->getHashServer($config::$servers);
        }
        
        while (true) {
            if (empty(self::$Flexihash->getAllTargets())) {
                \Logger::fatal('flexihash is empty');
                throw new \Exception('fake');
            }
            
            // 获取key索对应的host
            $host = self::$Flexihash->lookup($key);
            
            $port = isset($config::$servers[$host]) ? $config::$servers[$host] : $config::PORT;
            
            if (isset(self::$connects[$host][$port])) {
                $cache = self::$connects[$host][$port];
                break;
            } else {
                $clazz = '\\' . $_ENV['CACHE_DRIVER'];
                $cache = new $clazz();
                
                // 是否长链接
                if ($_ENV['MEMCACHE_P']) {
                    $result = $cache->pconnect($host, $port);
                } else {
                    $result = $cache->connect($host, $port, 1);
                }
                
                if ($result !== false) {
                    self::$connects[$host][$port] = $cache;
                    break;
                }
                
                \Logger::fatal(' fail to connect %s, host:%s, port:%s ', Cache::CACHE_DRIVER, $host, $port);
                // 如果链接失败,则吧当前的host移除列表
                self::$Flexihash->removeTarget($host);
                
                continue;
            }
        }
        
        return $cache;
    }

    /**
     * 获取hash一致性 key 的server
     * 
     * @param unknown $key            
     * @throws \Exception
     * @return string
     */
    private function getHashServer($servers)
    {
        if (empty($servers) || ! is_array($servers)) {
            \Logger::fatal('cache config err,%s:', $servers);
            throw new \Exception('fake');
        }
        
        $flexihash = new Flexihash();
        
        // 增加权重配置
        foreach ($servers as $weight => $targets) {
            self::$Flexihash = $flexihash->addTargets($targets, intval($weight));
        }
    }

    /**
     * 根据键值获取数值
     *
     * @param string $key
     *            键值
     * @param int $flag
     *            键值对应的flag
     * @return mixed 得到的值，如果没有返回null
     */
    public function get($key)
    {
        if (! is_null($value = $this->getCache($key)->get($key))) {
            return is_numeric($value) ? $value : unserialize($value);
        } else {
            return null;
        }
    }

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功
     */
    public function set($key, $value, $expiredTime = 0, $flag = 0)
    {
        $value = is_numeric($value) ? $value : serialize($value);
        
        $minutes = max(1, $expiredTime);
        
        $this->getCache($key)->set($key, $value, $flag, $minutes * 60);
    }

    public function forever($key, $value)
    {
        $value = is_numeric($value) ? $value : serialize($value);
        
        $this->getCache($key)->set($key, $value);
    }

    /**
     * Increment the value of an item in the cache.
     *
     * @param string $key            
     * @param mixed $value            
     * @return int|bool
     */
    public function increment($key, $value = 1)
    {
        return $this->memcache->increment($this->prefix . $key, $value);
    }

    /**
     * Decrement the value of an item in the cache.
     *
     * @param string $key            
     * @param mixed $value            
     * @return int|bool
     */
    public function decrement($key, $value = 1)
    {
        return $this->memcache->decrement($this->prefix . $key, $value);
    }

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功, NOT_STORED 表示存储不成功
     */
    public function add($key, $value, $expiredTime = 0, $flag = 0)
    {
        $this->set($key, $value, $expiredTime, $flag);
    }

    /**
     * 删除一个缓存键值
     *
     * @param string $key
     *            要删除的键值
     * @return bool 是否成功
     */
    public function del($key)
    {
        $this->getCache($key)->del($key);
    }
}
/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
