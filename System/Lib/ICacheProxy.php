<?php
namespace System\Lib;

interface ICacheProxy
{

    /**
     * 获取当前cache对象
     *
     * @throws \Exception
     * @return cache
     */
    public function getCache($key);

    /**
     * 根据键值获取数值
     *
     * @param string $key
     *            键值
     * @param int $flag
     *            键值对应的flag
     * @return mixed 得到的值，如果没有返回null
     */
    public function get($key);

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功
     */
    public function set($key, $value, $expiredTime = 0, $flag = 0);

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功, NOT_STORED 表示存储不成功
     */
    public function add($key, $value, $expiredTime = 0, $flag = 0);

    /**
     * 删除一个缓存键值
     *
     * @param string $key
     *            要删除的键值
     * @return bool 是否成功
     */
    public function del($key);

    /**
     *
     * @param unknown $key            
     * @param number $value            
     */
    public function increment($key, $value = 1);

    public function decrement($key, $value = 1);
}
/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
