<?php
namespace System\Lib;

use System\Lib;

/**
 * 语言类
 * 
 * @author machao
 *        
 */
class Language
{

    protected $_string = array();

    private static $ins = null;

    /**
     * 获取实例
     * 
     * @return Language
     */
    public static function getInstance()
    {
        if (self::$ins == null) {
            self::$ins = new self();
        }
        
        return self::$ins;
    }

    /**
     * 单例
     * 
     * @throws Exception
     */
    private function __construct()
    {
        $file = _ROOT . '/App/Language/' . $_ENV['APP_LOCALE'] . '.php';
        
        if (! file_exists($file)) {
            Logger::fatal('not found language file');
            throw new \Exception('fake');
        }
        
        $languages = require_once $file;
        
        if (! isset($languages) || ! is_array($languages)) {
            throw new \Exception('not found languages in file');
        }
        
        $this->_string = $languages;
    }

    /**
     * 获取语言对应内容
     * 
     * @param unknown $name            
     * @return multitype:|NULL
     */
    public function getString($name)
    {
        if (isset($this->_string[$name])) {
            return $this->_string[$name];
        } else {
            Logger::warning('not found %s to language file', $name);
            return $name;
        }
    }

    /**
     * 设置语言对应内容
     * 
     * @param unknown $name            
     * @param unknown $value            
     * @return boolean
     */
    public function setString($name, $value)
    {
        if (isset($this->_string[$name])) {
            Logger::warning('name is exists:%s', $name);
            return false;
        } else {
            $this->_string[$name] = $value;
            return true;
        }
    }
}
