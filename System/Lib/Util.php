<?php
namespace System\Lib;

class Util
{

    /**
     * 将结果按指定的key进行索引
     *
     * @param array $arrData            
     * @param mixed $keyIndex            
     */
    static function arrayIndex($arrData, $keyIndex)
    {
        $arrRet = array();
        foreach ($arrData as $arrRow) {
            $arrRet[$arrRow[$keyIndex]] = $arrRow;
        }
        return $arrRet;
    }

    /**
     * 将结果按指定的key/value进行索引
     *
     * @param array $arrData            
     * @param mixed $keyIndex            
     * @param mixed $valueIndex            
     */
    static function arrayIndexCol($arrData, $keyIndex, $valueIndex)
    {
        $arrRet = array();
        foreach ($arrData as $arrRow) {
            $arrRet[$arrRow[$keyIndex]] = $arrRow[$valueIndex];
        }
        return $arrRet;
    }

    /**
     * 从结果中抽取一列出来形成新array
     *
     * @param array $arrData            
     * @param mixed $keyIndex            
     */
    static function arrayExtract($arrData, $keyIndex)
    {
        $arrRet = array();
        foreach ($arrData as $arrRow) {
            $arrRet[] = $arrRow[$keyIndex];
        }
        return $arrRet;
    }

    /**
     * 解密cookie信息
     *
     * @param unknown $string            
     * @param string $operation            
     * @param string $key            
     * @param number $expiry            
     * @return string
     */
    public static function ED_authcode($string, $operation = 'DECODE', $key = '', $expiry = 0)
    {
        $ckey_length = 4;
        
        $key = md5($key ? $key : PlatformDef::SYS_COOKIE_KEY);
        $keya = md5(substr($key, 0, 16));
        $keyb = md5(substr($key, 16, 16));
        $keyc = $ckey_length ? ($operation == 'DECODE' ? substr($string, 0, $ckey_length) : substr(md5(microtime()), - $ckey_length)) : '';
        
        $cryptkey = $keya . md5($keya . $keyc);
        $key_length = strlen($cryptkey);
        
        $string = $operation == 'DECODE' ? base64_decode(substr($string, $ckey_length)) : sprintf('%010d', $expiry ? $expiry + time() : 0) . substr(md5($string . $keyb), 0, 16) . $string;
        $string_length = strlen($string);
        
        $result = '';
        $box = range(0, 255);
        
        $rndkey = array();
        for ($i = 0; $i <= 255; $i ++) {
            $rndkey[$i] = ord($cryptkey[$i % $key_length]);
        }
        
        for ($j = $i = 0; $i < 256; $i ++) {
            $j = ($j + $box[$i] + $rndkey[$i]) % 256;
            $tmp = $box[$i];
            $box[$i] = $box[$j];
            $box[$j] = $tmp;
        }
        
        for ($a = $j = $i = 0; $i < $string_length; $i ++) {
            $a = ($a + 1) % 256;
            $j = ($j + $box[$a]) % 256;
            $tmp = $box[$a];
            $box[$a] = $box[$j];
            $box[$j] = $tmp;
            $result .= chr(ord($string[$i]) ^ ($box[($box[$a] + $box[$j]) % 256]));
        }
        
        if ($operation == 'DECODE') {
            if ((substr($result, 0, 10) == 0 || substr($result, 0, 10) - time() > 0) && substr($result, 10, 16) == substr(md5(substr($result, 26) . $keyb), 0, 16)) {
                return substr($result, 26);
            } else {
                return '';
            }
        } else {
            return $keyc . str_replace('=', '', base64_encode($result));
        }
    }

    /**
     * 判断两个时间是否是同一天
     *
     * @param int $checkTime
     *            需要检查的时刻
     * @param int $offset
     *            策划们配置的偏移量，哪个时刻算新的 一天
     */
    static function isSameDay($checkTime, $offset = 0)
    {
        
        // 获取当前时间
        $referenceTime = self::getTime();
        // 两者都减去偏移量
        $referenceTime -= $offset;
        $checkTime -= $offset;
        // 如果检查时间小于判定时刻
        if (date("Y-m-d ", $checkTime) === date("Y-m-d ", $referenceTime)) {
            // 尚未通过这个时刻，返回 TRUE
            return true;
        }
        // 已经通过这个时刻，返回 FALSE
        return false;
    }

    /**
     * 查看两个时间段之间间隔了多少天
     *
     * @param int $checkTime
     *            需要检查的时刻
     * @param int $offset
     *            策划们配置的偏移量，哪个时刻算新的 一天
     */
    static function getDaysBetween($checkTime, $offset = 0)
    {
        
        // 获取当前时间
        $referenceTime = WebContext::getInstance()->getRequestTime();
        // 两者都减去偏移量
        $referenceTime -= $offset;
        $checkTime -= $offset;
        // 一天的秒数
        $SECONDS_OF_DAY = 86400;
        
        $ret = intval((strtotime(date("Y-m-d ", $referenceTime)) - strtotime(date("Y-m-d ", $checkTime))) / $SECONDS_OF_DAY);
        
        \Logger::debug("getDaysBetween check time is %d, now is %d, ret is %d.", $checkTime, $referenceTime, $ret);
        return $ret;
    }

    /**
     * 判断两个时间是否在同一周
     * Enter description here .
     *
     *
     *
     * ..
     *
     * @param unknown_type $checkTime            
     * @param unknown_type $offset            
     */
    static function isSameWeek($checkTime, $offset = 0)
    {
        $curTime = self::getTime();
        $SECONDS_OF_WEEK = 604800;
        
        // 这个时间为周日的晚上
        // $s = "1970-3-1 23:59:59";
        // $BASE_TIME = strtotime($s);
        $BASE_TIME = 5155199;
        $checkTime -= ($BASE_TIME + $offset);
        $curTime -= ($BASE_TIME + $offset);
        
        $checkWeek = intval($checkTime / $SECONDS_OF_WEEK);
        $curWeek = intval($curTime / $SECONDS_OF_WEEK);
        
        return $checkWeek == $curWeek;
    }

    /**
     * 判断两个时间是否是同一个月
     * Enter description here .
     *
     *
     *
     * ..
     *
     * @param unknown_type $checkTime            
     * @param unknown_type $offset            
     */
    static function isSameMonth($checkTime, $offset = 0)
    {
        $curTime = WebContext::getInstance()->getRequestTime();
        // 32*24*3600
        $SECONDS_OF_32DAY = 2764800;
        
        // 大于32天，肯定不是同一个月
        if (abs($curTime - $SECONDS_OF_32DAY) > $SECONDS_OF_32DAY) {
            return false;
        }
        
        // 减去时间差得到当前是第几月
        $month = strftime("%m", $checkTime - $offset);
        $curMonth = strftime("%m", $curTime - $offset);
        
        if ($month == $curMonth) {
            return true;
        }
        return false;
    }

    /**
     * 返回年月日
     * 格式：20111015
     */
    static function todayDate($offset = 0)
    {
        $curTime = WebContext::getInstance()->getRequestTime();
        return strftime("%Y%m%d", $curTime - $offset);
    }

    /**
     * 生成logid
     */
    static function genLogId()
    {
        $code = microtime(TRUE) . '#' . rand(0, 9999);
        $ret = md5($code);
        $high = hexdec(substr($ret, 0, 16));
        $low = hexdec(substr($ret, 16));
        $ret = (($high ^ $low) & 0xFFFFFFFFFFFFFF) * 100;
        $ret %= 10000000000;
        if ($ret < 1000000000) {
            $ret += 1000000000;
        }
        
        return $ret;
    }

    /*
     * 返回当前时间微秒数
     */
    public static function microtime_float()
    {
        list ($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * 显示星期
     *
     * @param sting $gdate            
     * @param int $first            
     * @return array
     */
    static function aweek($gdate = "", $first = 0)
    {
        if (! $gdate)
            $gdate = date("Y-m-d");
        $w = date("w", strtotime($gdate));
        $dn = $w ? $w - $first : 6;
        $st = strtotime("$gdate -" . $dn . " days");
        $en = $st + (86400 * 7);
        return array(
            $st,
            $en
        );
    }

    /**
     * 二分范围查找
     *
     * @param array $arrWeightRange
     *            要查找的权重区间
     * @param number $weight
     *            要查找的权重
     * @return 查找到的key值
     * @throws Exception
     */
    public static function binaryRangeSearch($arrWeightRange, $weight)
    {
        $leftIndex = 0;
        $rightIndex = count($arrWeightRange) - 1;
        while ($leftIndex <= $rightIndex) {
            $currIndex = intval(($leftIndex + $rightIndex) / 2);
            
            if ($arrWeightRange[$currIndex][1] > $weight) {
                $rightIndex = $currIndex - 1;
            } else 
                if ($arrWeightRange[$currIndex][2] < $weight) {
                    $leftIndex = $currIndex + 1;
                } else {
                    return $arrWeightRange[$currIndex][0];
                }
        }
        \Logger::fatal("can't %d in array:%s", $weight, $arrWeightRange);
        throw new \Exception('sys');
    }

    /**
     * 检查名字是否合法。
     * 只支持utf-8中文、大小写字母、数字、下划线
     * 修改为不支持一些特殊字符，支持"~"
     *
     * @param string $name            
     * @return string 返回如下：
     *         ok
     *         invalid_char 有无效字符
     *         sensitive_word 有敏感词
     *        
     */
    public static function checkName($name)
    {
        
        // utf-8中文、大小写字母、数字、下划线
        // $reg = "/[^\x{4e00}-\x{9fa5}a-zA-Z0-9_]/u";
        $reg = "/[`!#\$%^&*()\\-+={}\\[\\]\\\\|:;'\"<>,\\/\\? \t]/u";
        if (preg_match($reg, $name)) {
            // 非法字符
            return 'invalid_char';
        } else {
            $ret = TrieFilter::search($name);
            
            if (empty($ret)) {
                return 'ok';
            } else {
                // 敏感字符
                return 'sensitive_word';
            }
        }
    }

    /**
     * 检查两个时间是否相同
     *
     * @param int $time1            
     * @param int $time2            
     * @return bool 时间相同则返回true
     */
    public static function isSameTime($time1, $time2)
    {
        return abs($time1 - $time2) == 0;
    }

    /**
     * 验证IP地址是否有效
     *
     * @param
     *            $ip_address
     */
    public static function ipCheckIsTrue($ip_address)
    {
        $ip = ip2long($ip_address);
        if ($ip == "-1" || $ip === FALSE) {
            
            return FALSE;
        }
        
        return TRUE;
    }

    /**
     * 检测参数并返回
     *
     * @param unknown $arrRequest            
     * @param unknown $key            
     * @param unknown $type            
     * @param string $strict            
     * @throws Exception
     * @return NULL|number|string|boolean
     */
    static function getParameter($arrRequest, $key, $type = 'string', $strict = true)
    {
        if (! isset($arrRequest[$key]) || (empty($arrRequest[$key]) && $arrRequest[$key] != 0)) {
            if ($strict) {
                \Logger::warning("key:%s not found in request", $key);
                throw new \Exception('fake', 195);
            }
            switch ($type) {
                case 'string':
                    return '';
                case 'bool':
                    return false;
                case 'float':
                    return 0.0;
                case 'int':
                    return 0;
                case 'array':
                    return array();
                default:
                    return null;
            }
        }
        
        $value = $arrRequest[$key];
        switch ($type) {
            case 'int':
                return intval($value);
            case 'string':
                return trim($value);
            case 'bool':
                return ! empty($value);
            case 'float':
                return floatval($value);
            case 'array':
                return is_array($value) ? $value : array();
            default:
                \Logger::warning("unsupported type:%s", $type);
                throw new \Exception('inter');
        }
    }

    /**
     * 获取随机数
     *
     * @param int $max            
     *
     * @return number
     */
    static function getRandNum($max = 4)
    {
        $new_number = '';
        srand(microtime() * 100000);
        for ($i = 0; $i < $max; $i ++) {
            $new_number .= dechex(rand(0, 15));
        }
        return $new_number;
    }

    /**
     * 随机返回一串字符串
     *
     * @param unknown $length            
     * @return string
     */
    static function getRandomString($length = 4)
    {
        $template = "123456789abcdefghijkmnpqrstuvwxyz";
        $randStr = '';
        
        for ($a = 0; $a < $length; $a ++) {
            
            $b = rand(0, strlen($template) - 1);
            
            $randStr .= $template[$b];
        }
        return $randStr;
    }

    /**
     * 预定义字符、数组中添加反斜杠
     *
     * @param string|array $string            
     * @return $string
     */
    static function saddslashes($string)
    {
        if (is_array($string)) {
            foreach ($string as $key => $val) {
                $string[$key] = self::saddslashes($val);
            }
        } else {
            $string = addslashes($string);
        }
        return $string;
    }

    /**
     * 查询IP
     *
     * @param array $arr            
     * @param unknown $target            
     * @return array|boolen
     */
    static function binarySearchIP(Array $arr, $target)
    {
        $low = 0;
        $high = count($arr) - 1;
        
        while ($low <= $high) {
            $mid = floor(($low + $high) / 2);
            if (isset($arr[$mid])) {
                $_tinfo = explode(' ', $arr[$mid]);
                if (count($_tinfo) == 3) {
                    if (self::compareIp($_tinfo[0], $target) == 0)
                        return $_tinfo[2];
                    if (self::compareIp($_tinfo[1], $target) == 0)
                        return $_tinfo[2];
                    if (self::compareIp($_tinfo[0], $target) == - 1 && self::compareIp($_tinfo[1], $target) == 1)
                        return $_tinfo[2];
                    if (self::compareIp($_tinfo[0], $target) == 1)
                        $high = $mid - 1;
                    if (self::compareIp($_tinfo[1], $target) == - 1)
                        $low = $mid + 1;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    /**
     * 比较IP
     *
     * @param string $ip1            
     * @param string $ip2            
     * @return array|boolen
     */
    static function compareIp($ip1, $ip2)
    {
        $ret = 0;
        $temp1 = explode(".", $ip1);
        $temp2 = explode(".", $ip2);
        for ($i = 0; $i < 4; $i ++) {
            if ($temp1[$i] > $temp2[$i]) {
                $ret = 1;
                break;
            } else 
                if ($temp1[$i] < $temp2[$i]) {
                    $ret = - 1;
                    break;
                }
        }
        return $ret;
    }

    /**
     * 将url归一化，即将多余的/去除，比如"//abc//def/b/"修改为"/abc/def/b"
     *
     * @param string $url            
     * @return string 归一化后的url
     */
    public static function uniformUri($uri)
    {
        if (empty($uri) || $uri[0] == '/') {
            \Logger::fatal("invalid uri:%s", $uri);
            throw new \Exception('fake');
        }
        
        $newUri = '';
        $len = strlen($uri);
        $dup = false;
        for ($i = 0; $i < $len; $i ++) {
            if ($uri[$i] == '?') {
                break;
            }
            
            if ($uri[$i] == '/') {
                if ($dup) {
                    continue;
                }
                $dup = true;
            } else {
                $dup = false;
            }
            $newUri = $newUri . $uri[$i];
        }
        
        $len = strlen($newUri);
        if ($newUri[$len - 1] == '/' && $len != 1) {
            $newUri = substr($newUri, 0, $len - 1);
        }
        return $newUri;
    }
}


/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
