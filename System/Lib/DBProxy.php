<?php
namespace System\Lib;

use Dibi\Connection;

class DBProxy
{

    private static $db = null;

    private static function getDB($new = null)
    {
        if (! (self::$db instanceof self) || ! is_null($new)) {
            $config['driver'] = $_ENV['DB_DRIVER'];
            $config['dsn'] = $_ENV['DB_HOST'];
            $config['username'] = $_ENV['DB_USERNAME'];
            $config['password'] = $_ENV['DB_PASSWORD'];
            self::$db = new Connection($config);
        }
        
        return self::$db;
    }
}