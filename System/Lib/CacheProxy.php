<?php
namespace System\Lib;

class CacheProxy
{

    protected function getClazz()
    {
        $clazz = '\System\Lib\\' . $_ENV['CACHE_DRIVER'] . 'Proxy';
        return new $clazz();
    }

    /**
     * 根据键值获取数值
     *
     * @param string $key
     *            键值
     * @param int $flag
     *            键值对应的flag
     * @return mixed 得到的值，如果没有返回null
     */
    protected function get($key)
    {
        return $this->getClazz()->get($this->getCacheKey($key));
    }

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功
     */
    protected function set($key, $value, $expiredTime = 0, $flag = 0)
    {
        return $this->getClazz()->set($this->getCacheKey($key), $value, $expiredTime, $flag);
    }

    /**
     * 向cache中设置一个变量
     *
     * @param string $key
     *            键值
     * @param mixed $value
     *            要设置的值，可以是array
     * @param int $expiredTime
     *            过期时间，0表示永远不过期
     * @param int $flag
     *            是否进行memcache压缩
     * @return string STORED 表示存储成功, NOT_STORED 表示存储不成功
     */
    protected function add($key, $value, $expiredTime = 0, $flag = 0)
    {
        return $this->getClazz()->add($this->getCacheKey($key), $value, $expiredTime, $flag);
    }

    /**
     * 删除一个缓存键值
     *
     * @param string $key
     *            要删除的键值
     * @return bool 是否成功
     */
    protected function del($key)
    {
        return $this->getClazz()->del($this->getCacheKey($key));
    }

    /**
     * 共同cacheKey定义getCacheKey
     *
     * @param string $key
     *            缓存名字
     */
    protected function getCacheKey($key)
    {
        return _ROOT . '|' . $key;
    }

    public static function __callStatic($method, $parameters)
    {
        $instance = new static();
        
        return call_user_func_array([
            $instance,
            $method
        ], $parameters);
    }
}
/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
