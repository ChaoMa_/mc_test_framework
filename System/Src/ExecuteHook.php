<?php
namespace System\Src;

class ExecuteHook
{

    /**
     * 执行hook操作
     * 
     * @param unknown $hooks            
     * @param unknown $arrRequest            
     */
    public static function executeHook($hooks, &$arrRequest)
    {
        foreach ($hooks as $hookModule => $hookClazz) {
            if (! is_numeric($hookModule) && $this->module != $hookModule) {
                continue;
            }
            
            $hookClazz = '\App\Hook\\' . $hookClazz;
            
            if ($hookClazz && ! class_exists($hookClazz)) {
                \Logger::fatal("class %s not exists", $hookClazz);
                throw new \Exception('class');
            }
            
            $action = 'execute';
            
            if (! method_exists($hookClazz, $action)) {
                \Logger::fatal("object has no method:%s in class:%s", $action, $hookClazz);
                throw new \Exception('method');
            }
            $caller = array(
                new $hookClazz(),
                $action
            );
            
            call_user_func_array($caller, array(
                $arrRequest
            ));
        }
    }
}