<?php
namespace System\Src;

use System\Config\Smarty;
use App\Conf\Hook;
use FastRoute\Dispatcher;

/**
 * 具体实现逻辑的类
 */
class WebFramework
{

    /**
     * 前端发过来的请求数据
     *
     * @var array 格式如下：
     *      <code>
     *      {
     *      key:value
     *      }
     *      </code>
     */
    protected $arrRequest;

    /**
     * 前端用户的ip
     *
     * @var string
     */
    protected $clientIp;

    /**
     * 请求开始时间
     *
     * @var float
     */
    protected $starTime;

    /**
     * 请求结束时间
     *
     * @var float
     */
    protected $endTime;

    /**
     * 方法结束时间
     *
     * @var int
     */
    protected $endMethodTime;

    /**
     * 方法开始时间
     *
     * @var int
     */
    protected $startMethodTime;

    /**
     * 当前的logid
     *
     * @var int
     */
    protected $logid;

    /**
     * 请求的方法名
     *
     * @var string
     */
    protected $uri;

    /**
     * 请求发过来的时间
     *
     * @var int
     */
    protected $requestTime;

    /**
     * 请求的域名host
     *
     * @var string
     */
    protected $host;

    /**
     * 请求方式 GET POST
     *
     * @var string
     */
    protected $method;

    /**
     * 请求过来的参数
     *
     * @var string
     */
    protected $params;

    /**
     * 请求过来的模块
     *
     * @var string
     */
    protected $module;

    /**
     * 是否在requestEnd里
     *
     * @var boolean
     */
    protected $request_start_end;

    /**
     * 需要延时执行的方法
     *
     * @var array
     */
    private $arrDelayMethod = array();

    private static $app = null;

    private $response = null;

    private $route = array();

    private $class = null;

    private $Tpl = null;

    /**
     * 构造函数
     */
    private function __construct()
    {
        $this->starTime = microtime(true);
        $this->endTime = 0;
        $this->startMethodTime = microtime(true);
        $this->endMethodTime = 0;
        $this->clientIp = "";
        $this->uri = "unknown";
        $this->requestTime = time();
        $this->method = 'GET';
    }

    public static function getInstance()
    {
        if (empty(self::$app)) {
            self::$app = new self();
        }
        return self::$app;
    }

    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    public function __get($val)
    {
        return $this->$val;
    }

    /**
     * 开始服务
     */
    public function start()
    {
        ob_start();
        set_error_handler(array(
            $this,
            'errorHandler'
        ));
        try {
            $this->initRequest();
            \Logger::addBasic('logid', $this->logid);
            \Logger::addBasic('client', $this->clientIp);
            \Logger::addBasic('method', $this->method);
            \Logger::addBasic('uri', $this->uri);
            \Logger::info("request url:%s,paramters %s", $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], $this->arrRequest);
            
            // 载入hook
            ExecuteHook::executeHook(Hook::$before, $this->arrRequest);
            
            ExecuteHook::executeHook(Hook::$module_before, $this->arrRequest);
            
            // 开始程序
            try {
                $arrRet = $this->executeMethod();
            } catch (\Exception $e) {
                throw $e;
            } finally
			{
                $this->endMethodTime = microtime(true);
            }
            
            $err = 'ok';
            
            // 结束hook
            ExecuteHook::executeHook(Hook::$module_after, $arrRet);
            ExecuteHook::executeHook(Hook::$after, $arrRet);
        } catch (\Exception $e) {
            $this->endMethodTime = microtime(true);
            $err = $e->getMessage();
            if (($err != 'fake' && $err != 'dummy') || $_ENV['APP_DEBUG']) {
                \Logger::fatal("uncaught Exception:%s,err code:%s,err file:%s,err line:%s", $e->getMessage(), $e->getCode(), $e->getFile(), $e->getLine());
                \Logger::info("%s", $e->getTraceAsString());
            }
            $arrRet = null;
        }
        
        $this->requestEnd($err, $arrRet);
    }

    private function initRequest()
    {
        $this->clientIp = $this->getIp();
        
        $parameters = $this->getRequestParameter();
        
        $this->arrRequest = $parameters['GET'] + $parameters['POST'];
        
        $this->logid = $this->genLogId();
        $uri = $_SERVER['REQUEST_URI'];
        
        if (false !== $pos = strpos($_SERVER['REQUEST_URI'], '?')) {
            $uri = substr($uri, 0, $pos);
        }
        $this->uri = rawurldecode($uri);
        
        $this->method = $_SERVER['REQUEST_METHOD'];
        
        $dispatcher = \FastRoute\simpleDispatcher(function (\FastRoute\RouteCollector $r) {
            foreach ($this->route as $key => $value) {
                $r->addRoute(strtoupper($value[0]), $value[1], $value[2]);
            }
        });
        
        $routeInfo = $dispatcher->dispatch($this->method, $this->uri);
        
        switch ($routeInfo[0]) {
            case Dispatcher::FOUND:
                
                if (count($_tmp = explode('\\', $routeInfo[1])) >= 2) {
                    $this->module = array_pop($_tmp);
                } else {
                    $this->module = $routeInfo[1];
                }
                
                $this->class = substr($routeInfo[1], 0, strrpos($routeInfo[1], '\\'));
                $this->params = $routeInfo[2];
                break;
            case Dispatcher::NOT_FOUND:
            case Dispatcher::METHOD_NOT_ALLOWED:
            default:
                $this->NotFound();
        }
    }

    public function addRoute($model, $route, $handler)
    {
        $this->route[] = array(
            $model,
            $route,
            $handler
        );
    }

    private function executeMethod()
    {
        $clazz = '\App\Controllers\\' . $this->class . '\\' . $this->class;
        
        if ($clazz && ! class_exists($clazz)) {
            \Logger::fatal("class %s not exists", $clazz);
            throw new \Exception('class');
        }
        
        $object = new $clazz(self::$app);
        
        if (! method_exists($object, $this->module)) {
            \Logger::fatal("object has no method:%s in class:%s", $this->module, $clazz);
            throw new \Exception('method');
        }
        
        $caller = array(
            $object,
            $this->module
        );
        
        $ret = call_user_func_array($caller, array(
            $this->params
        ));
        
        \Logger::debug("call method done");
        
        return $ret;
    }

    public function setText()
    {
        $this->response = 'text';
    }

    public function setJson()
    {
        $this->response = 'json';
    }

    public function setSerialize()
    {
        $this->response = 'serialize';
    }

    public function getResponseType()
    {
        return $this->response == null ? 'tpl' : $this->response;
    }

    public function setRedirect()
    {
        $this->response = 'redirect';
    }

    public function getTpl()
    {
        return $this->Tpl == null ? '' : $this->Tpl;
    }

    public function setTpl($tpl)
    {
        $this->Tpl = $tpl;
    }

    public function setRedirectUrl($tpl)
    {
        $this->Tpl = $tpl;
    }

    public function getRedirectUrl()
    {
        return $this->Tpl == null ? '' : $this->Tpl;
    }

    /**
     * 返回数据处理
     * 
     * @param unknown $err            
     * @param unknown $ret            
     * @throws \Exception
     */
    private function sendResponse($err, $ret = null)
    {
        ob_end_clean();
        $responseType = $this->getResponseType();
        
        if ($err != 'ok' && empty($ret)) {
            $this->NotFound();
        }
        
        \Logger::debug('responseType:%s', $responseType);
        
        \Logger::debug('response:%s', $ret);
        
        switch ($responseType) {
            
            case 'tpl':
                $oldLevel = error_reporting(E_ALL | ~ E_NOTICE);
                
                if (! isset($ret['title']) && is_array($ret)) {
                    $ret['title'] = '';
                }
                
                $tpl = $this->getTpl();
                $smarty = new \Smarty();
                $smarty->setCompileDir(_ROOT . '/' . $_ENV['SMARTY_COMPILE_DIR']);
                $smarty->setLeftDelimiter($_ENV['SMARTY_LEFT_DELIMITER']);
                $smarty->setRightDelimiter($_ENV['SMARTY_REGHT_DELIMITER']);
                $smarty->setCacheLifetime($_ENV['SMARTY_CACHE_LEFTTIME']);
                $smarty->setCaching($_ENV['SMARTY_CACHEING']);
                $smarty->assign('p', $ret);
                try {
                    if (empty($tpl)) {
                        $tpl = $this->class . '/' . $this->module . '.tpl';
                    }
                    
                    $tpl = _ROOT . $_ENV['SMARTY_TEMPLATE'] . '/' . $tpl;
                    
                    if (! file_exists($tpl)) {
                        \Logger::fatal("%s,tpl not exists", $tpl);
                        throw new \Exception('sys');
                    }
                    
                    $smarty->display($tpl);
                    return;
                } catch (\Exception $e) {
                    \Logger::fatal("uncaught smarty \Exception:%s", $e->getMessage());
                }
                error_reporting($oldLevel);
                break;
            
            case 'json':
                $data = json_encode($ret);
                echo $data;
                return;
            
            case 'redirect':
                header('Location: ' . $this->getRedirectUrl());
                return;
            
            case 'text':
                if (! is_string($ret)) {
                    \Logger::fatal('err text:%s', $ret);
                    throw new \Exception('response ret err not text');
                } else {
                    echo $ret;
                }
                return;
            
            case 'serialize':
                echo serialize($ret);
                return;
            
            default:
                \Logger::fatal("unsupported response type:%s", $responseType);
        }
        
        $this->NotFound();
    }

    public function NotFound()
    {
        \Logger::fatal("page not found");
        header('Status: 404 Not Found');
        exit();
    }

    private function requestEnd($err, $ret)
    {
        $this->request_start_end = true;
        $this->sendResponse($err, $ret);
        $this->endTime = microtime(true);
        $totalCost = intval(($this->endTime - $this->starTime) * 1000);
        $methodCost = intval(($this->endMethodTime - $this->startMethodTime) * 1000);
        $frameCost = intval($totalCost - $methodCost);
        if ($totalCost > $_ENV['APP_MAX_EXECUTE_TIME']) {
            \Logger::fatal('method:%s execute time:%d(ms) is too long', $this->uri, $totalCost);
        }
        \Logger::notice("err:%s, total cost:%d(ms), framework cost:%d(ms), method cost:%d(ms)", $err, $totalCost, $frameCost, $methodCost);
        
        $arrDelayMethod = $this->arrDelayMethod;
        if (empty($arrDelayMethod)) {
            return;
        }
        
        if (! function_exists('fastcgi_finish_request')) {
            \Logger::fatal("fastcgi_finish_request not supported, delay methods will be discarded");
            return;
        }
        
        fastcgi_finish_request();
        foreach ($arrDelayMethod as $arrMethod) {
            $delay = $arrMethod['delay'];
            $method = $arrMethod['method'];
            $args = $arrMethod['args'];
            if (! empty($delay)) {
                usleep($delay);
            }
            call_user_func_array($method, $args);
        }
    }

    private function getClassLocation($module, $method, $exmethod)
    {
        if (strtoupper($method) == 'POST') {
            $file = _ROOT . "/App/Controllers/$module/commit.index.php";
        } else {
            $file = _ROOT . "/App/Controllers/$module/browse.index.php";
        }
        
        return $file;
    }

    public function errorHandler($errcode, $errstr, $errfile, $errline, $errcontext)
    {
        if (! ($errcode & error_reporting())) {
            return true;
        }
        $this->endMethodTime = microtime(true);
        \Logger::fatal('errcode:%d, errstr:%s, errfile:%s, errline:%s', $errcode, $errstr, $errfile, $errline);
        \Logger::debug('error context:%s', $errcontext);
        
        if ($this->request_start_end == false) {
            $this->requestEnd('php', null);
        }
        exit(0);
    }

    /**
     * 预定义字符、数组中添加反斜杠
     *
     * @param string|array $string            
     * @return $string
     */
    private function saddslashes($string)
    {
        if (is_array($string)) {
            foreach ($string as $key => $val) {
                $string[$key] = self::saddslashes($val);
            }
        } else {
            $string = addslashes($string);
        }
        return $string;
    }

    /**
     * 接收参数处理
     *
     * @return array
     */
    private function getRequestParameter()
    {
        if (PHP_VERSION < '4.1.0') {
            $_GET = &$HTTP_GET_VARS;
            $_POST = &$HTTP_POST_VARS;
            $_COOKIE = &$HTTP_COOKIE_VARS;
            $_SERVER = &$HTTP_SERVER_VARS;
            $_ENV = &$HTTP_ENV_VARS;
            $_FILES = &$HTTP_POST_FILES;
        }
        
        if (! empty($postReader = file_get_contents('php://input'))) {
            parse_str($postReader, $_POST);
        }
        
        if (! (get_magic_quotes_gpc())) {
            $_GET = $this->saddslashes($_GET);
            $_POST = $this->saddslashes($_POST);
            $_COOKIE = $this->saddslashes($_COOKIE);
        }
        
        return array(
            'GET' => $_GET,
            'POST' => $_POST,
            'COOKIE' => $_COOKIE
        );
    }

    /**
     * 获取当前ip地址
     *
     * @return Ambigous <string, unknown>
     */
    private function getIp()
    {
        $realip = '0.0.0.0';
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $arr = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            
            if ($arr[0] != $_SERVER['REMOTE_ADDR']) {
                
                $realip = $_SERVER['REMOTE_ADDR'];
            } else {
                
                /* 取X-Forwarded-For中第一个非unknown的有效IP字符串 */
                foreach ($arr as $ip) {
                    $ip = trim($ip);
                    if ($ip != 'unknown') {
                        $realip = $ip;
                        break;
                    }
                }
                $reallongip = ip2long($realip);
                $net_a = ip2long('10.255.255.255') >> 24; // A类网预留ip的网络地址
                $net_b = ip2long('172.31.255.255') >> 20; // B类网预留ip的网络地址
                $net_c = ip2long('192.168.255.255') >> 16; // C类网预留ip的网络地址
                if ($reallongip >> 24 === $net_a || $reallongip >> 20 === $net_b || $reallongip >> 16 === $net_c) {
                    if (isset($_SERVER['HTTP_CLIENT_IP'])) {
                        $realip = $_SERVER['HTTP_CLIENT_IP'];
                    } else 
                        if (isset($_SERVER['REMOTE_ADDR'])) {
                            $realip = $_SERVER['REMOTE_ADDR'];
                        }
                }
            }
        } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $realip = $_SERVER['HTTP_CLIENT_IP'];
        } else 
            if (isset($_SERVER['REMOTE_ADDR'])) {
                $realip = $_SERVER['REMOTE_ADDR'];
            }
        
        return $realip;
    }

    /**
     * 添加临时执行的方法
     *
     * @param mixed $method
     *            要执行的方法，符合php的callback要求
     * @param array $args
     *            方法对应的参数
     * @param int $delay
     *            临时执行的时间,单位为us
     */
    public function addDelayMethod($method, $args, $delay = 0)
    {
        if (! is_array($args)) {
            Logger::fatal("args should be array");
            throw new Exception('inter');
        }
        
        if (is_string($method)) {
            if (! function_exists($method)) {
                Logger::fatal("function:%s not found", $method);
                throw new Exception('inter');
            }
        } else 
            if (is_array($method)) {
                if (empty($method[0]) || empty($method[1]) || ! method_exists($method[0], $method[1])) {
                    Logger::fatal("method:%s not found", $method);
                    throw new Exception('inter');
                }
            } else {
                Logger::fatal("invalid method format:%s", $method);
                throw new Exception('inter');
            }
        
        $this->arrDelayMethod[] = array(
            'method' => $method,
            'args' => $args,
            'delay' => intval($delay)
        );
    }

    /**
     * 生成logid
     */
    private function genLogId()
    {
        $code = microtime(TRUE) . '#' . rand(0, 9999);
        $ret = md5($code);
        $high = hexdec(substr($ret, 0, 16));
        $low = hexdec(substr($ret, 16));
        $ret = (($high ^ $low) & 0xFFFFFFFFFFFFFF) * 100;
        $ret %= 10000000000;
        if ($ret < 1000000000) {
            $ret += 1000000000;
        }
        
        return $ret;
    }
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
