# 框架使用说明
##初始化
```
php composer install
```


##框架结构说明
```
入口文件:
			public/index.php
资源目录:
			Resources/data	--用于存放数据文件
						/log	--log目录
						/docs	--文档目录
系统目录:
			System/Lib	--系统扩展
					/Src	--框架目录
视图目录:
			views/	用于存放视图文件
主开发目录:
			App/Conf	--配置文件
				/Controllers	--控制器目录
				/Hook	--过滤器目录
				/Language	--语言包目录
				/Modules		--数据层
				/Script	--脚本目录
				router.php	路由文件
```
## 使用说明
配置文件为根目录的.env文件内，如需改变配置,直接修改此隐藏文件即可。    
所有路由写在App/router.php内
## 代码覆盖率使用
启用代码覆盖率(COVERAGE=true)并(APP_DEBUG=true)的话，会自动记录代码执行覆盖情况， 执行脚本    
```
php public/index.php -f GenCoverage  
```

会在views下生成html形式的覆盖率情况，浏览器地址：http://xxx/coverage/index.html即可！
##脚本
###编写脚本
1. 脚本放到App\Script\文件夹内     
2. 类需要继承System\Lib\BaseScript     
3. 并重写public function executeScript($arrOption)方法
###执行脚本
php public/index.php -f 脚本名称

## 源码说明
* view  		smarty/smarty
* test 		phpunit/phpunit
* route		nikic/fast-route
* db			dg/dibi
* env 		josegonzalez/dotenv

##nginx配置
```
server {
        listen               80;
        index               index.php;
        set $root   /path/mc_test_framework;
        set $name   mc_test_framework;
        set $resources $root/Resources/;
        set $static $resources/static/;

        location / {
                fastcgi_pass   phpfpm:9000;
                include        fastcgi_params;
                fastcgi_param  SCRIPT_FILENAME  $root/public/index.php;
        }

        location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$
        {
                expires      30d;
        }

        location ~ .*\.(js|css)?$
        {
                expires      1h;
        }

        location ^~ /js {
                root           $static;
        }

        location ^~ /images {
                root           $static;
        }

        location ^~ /css {
                root           $static;
        }

        location ^~ /coverage {
                root           $views/;
        }

        location ^~ /static {
                root        $root;
        }
        
        location /favicon.ico {
                root           $root/static/;
        }

        access_log  logs/$name.log  main;
}
```

